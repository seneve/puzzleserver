<?php

namespace Puzzle\OAuthServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PuzzleOAuthServerBundle:Default:index.html.twig');
    }
}
