<?php 

namespace Puzzle\ApiBundle\Listener;

use Puzzle\ApiBundle\Event\MediaFileEvent;
use Puzzle\ApiBundle\Entity\MediaFile;
use Puzzle\ApiBundle\Event\MediaFolderEvent;
use Puzzle\ApiBundle\Service\MediaManager;
use Doctrine\ORM\EntityManagerInterface;
use Puzzle\ApiBundle\Util\StringUtil;
use Puzzle\ApiBundle\Service\MediaUploader;
use Puzzle\ApiBundle\Entity\MediaFolder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 *
 */
class MediaListener
{
	/**
	 * @var EntityManagerInterface
	 */
	private $em;
	
	/**
	 * @var MediaManager
	 */
	private $mediaManager;
	
	/**
	 * @var MediaUploader
	 */
	private $mediaUploader;
	
	public function __construct(EntityManagerInterface $em, MediaManager $mediaManager, MediaUploader $mediaUploader){
		$this->em = $em;
		$this->mediaManager = $mediaManager;
		$this->mediaUploader = $mediaUploader;
	}
	
	/**
	 * Folder created on disk
	 *
	 * @param MediaFolderEvent $event
	 * @return boolean
	 */
	public function onCreateFolder(MediaFolderEvent $event) {
	    $folder = $event->getFolder();
	    $data = $event->getData();
	    $user = $data['user'];
	    
	    $parent = $folder->getParent();
	    $parent = $parent ??  $this->em->getRepository(MediaFolder::class)->findOneBy([
	        'name'    => $user->getUsername(),
	        'user'    => $user->getId()
	    ]);
	    
	    if (! $parent){
	        $parent = new MediaFolder();
	        $parent->createDefault($user);
	        $this->em->persist($parent);
	    }
	    
	    $folder->setParent($parent);
	    $this->em->flush($folder);
	    
	    return $this->mediaManager->createFolder($folder, $user);
	}
	
	/**
	 * Rename folder on disk
	 *
	 * @param MediaFolderEvent $event
	 * @return boolean
	 */
	public function onRenameFolder(MediaFolderEvent $event) {
	    $folder = $event->getFolder();
	    $data = $event->getData();
	    if (file_exists($folder->getAbsolutePath()) === false) {
	        $this->mediaManager->renameFolder($folder, $data['oldAbsolutePath']);
	    }
	    
	    $this->em->flush();
	    return true;
	}
	
	/**
	 * Remove folder on disk
	 *
	 * @param MediaFolderEvent $event
	 * @return boolean
	 */
	public function onRemoveFolder(MediaFolderEvent $event) {
	    $folder = $event->getFolder();
	    $folderPath = $folder->getAbsolutePath();
	    if (file_exists($folderPath) === true) {
	        $this->mediaManager->removeFolder($folderPath);
	    }
	    
	    return true;
	}
	
	/**
	 * Add files to folder
	 *
	 * @param MediaFolderEvent $event
	 */
	public function onAddFilesToFolder(MediaFolderEvent $event) {
	    $folder = $event->getFolder();
	    $data = $event->getData();
	    
	    $filesToAdd = $data['files_to_add'] ?? null;
	    $preserveFiles = $data['preserve_files'];
	    
	    if ($filesToAdd !== null) {
	        $files = is_string($filesToAdd) ? explode(',', $filesToAdd) : $filesToAdd;
	        $er = $this->em->getRepository(MediaFile::class);
	        
	        foreach ($files as $item){
	            if ($file = $er->findOneBy(['path' => $item]) || $file = $er->find($item)) {
	                if ($file instanceof MediaFile && file_exists($file->getAbsolutePath())) {
	                    if ($preserveFiles === false) {
	                       $this->mediaManager->moveFile($file, $folder);
	                    }else {
	                        $isOverwritable = $data['is_overwritable'] ?? false;
	                        $this->mediaManager->copyFile($file, $folder, $isOverwritable);
	                    }
	                }
	            }else {
	                if (StringUtil::isValidFromUrl($item)){
	                    $this->mediaUploader->uploadFromUrl($item, $folder->getId());
	                }
	            }
	        }
	    }
	    
	    return true;
	}
	
	/**
	 * Empty folder
	 *
	 * @param MediaFolderEvent $event
	 */
	public function onRemoveFilesFromFolder(MediaFolderEvent $event) {
	    $folder = $event->getFolder();
	    $data = $event->getData();
	    
	    if (isset($data['files_to_remove']) === true && $data['files_to_remove'] !== null) {
	        $filesId = $data['files_to_remove'];
	        $er = $this->em->getRepository(MediaFile::class);
	        foreach ($filesId as $fileId){
	            /** @var MediaFile $file */
	            $file = $er->find($fileId);
	            if ($file !== null && file_exists($file->getAbsolutePath())) {
	                unlink($folder->getPath().'/'.$file->getName());
	            }
	        }
	    }
	    
	    return true;
	}
	
	/**
	 * Post created on disk
	 * 
	 * @param MediaFileEvent $event
	 */
	public function onCreateFile(MediaFileEvent $event) {
	    $data = $event->getData();
		
		if (file_exists($data['absolutePath']) === false) {
		    return mkdir($data['absolutePath'], 0777, true);
		}
		
		return true;
	}
	
	/**
	 * Rename post on disk
	 *
	 * @param MediaFileEvent $event
	 */
	public function onRenameFile(MediaFileEvent $event) {
	    $data = $event->getData();
	    if (file_exists($data['absolutePath']) === false) {
	        return rename($data['oldAbsolutePath'], $data['absolutePath']);
	    }
	    
	    return true;
	}
	
	/**
	 * Remove file on disk
	 *
	 * @param MediaFileEvent $event
	 */
	public function onRemoveFile(MediaFileEvent $event) {
	    $data = $event->getData();
	    if (file_exists($data['absolutePath']) === true) {
	        unlink($data['absolutePath']);
	    }
	    
	    return true;
	}
	
	
	/**
	 * Copy or upload file from API's other than MediaFile
	 *
	 * @param MediaFileEvent $event
	 */
	public function onCopyFile(MediaFileEvent $event) {
	    $data = $event->getData();
	    $user = $data['user'];
	    
	    $erFolder = $this->em->getRepository(MediaFolder::class);
	    if (isset($data['folder']) && $data['folder']){
	        if (!$folder = $erFolder->find($data['folder']) && ! $folder = $erFolder->findOneBy([
	            'name'             => $data['folder'],
	            'user'             => $user->getId(),
	            'isOverwritable'   => false // App created by system
	        ])){
	            $folder = new MediaFolder();
	            $folder->setName($data['folder']);
	            $folder->setUser($user);
	            $folder->setOverwritable(false);
	            
	            $this->em->persist($folder);
	            $this->em->flush($folder);
	            
	            $folder = self::onCreateFolder(new MediaFolderEvent($folder, ['user' => $user]));
	        }
	    }else{
	        $folder = $this->em->getRepository(MediaFolder::class)->findOneBy([
	            'name'             => $user->getUsername(),
	            'user'             => $user->getId(),
	            'isOverwritable'   => false
	        ]);
	        
	        if ($folder === null) {
	            $folder = new MediaFolder();
	            $folder->createDefault($user);
	            
	            $this->em->persist($folder);
	            $this->em->flush($folder);
	            
	            $folder = $this->mediaManager->createFolder($folder, $user);
	        }
	    }

	    if ($folder !== null && isset($data['path']) && $data['path'] !== null) {
	        $er = $this->em->getRepository(MediaFile::class);
	        if ($file = $er->findOneBy(['path' => $data['path']]) || $file = $er->find($data['path'])) {
	            if ($file !== null && file_exists($file->getAbsolutePath())) {
	                if (isset($data['preserve_files']) === true && $data['preserve_files'] === false) {
	                    $this->mediaManager->moveFile($file, $folder);
	                }else {
	                    $isOverwritable = isset($data['is_overwritable']) ? $data['is_overwritable'] : false;
	                    $this->mediaManager->copyFile($file, $folder, $isOverwritable);
	                }
	            }
	        }else {
	            if (StringUtil::isValidFromUrl($data['path'])){
	                $response = $this->mediaUploader->uploadFromUrl($data['path'], $folder->getId());
	                
	                $file = new MediaFile();
	                $file->setName($response['name']);
	                $file->setExtension($response['extension']);
	                $file->setPath($folder->getPath().'/'.$response['name']);
	                $file->setSize(filesize($folder->getAbsolutePath().'/'.$response['name']));
	                $file->setUser($user);
	                
	                $this->em->persist($file);
	                
	                $folder->addFile($file->getId());
	                
	                $this->em->flush();
	            }
	        }
	        
	        $closure = $data['closure'] ?? null;
	        if ($closure){
	            $closure($folder->getPath().'/'.$file->getName());
	        }
	    }
	    
	    return true;
	}
}

?>
