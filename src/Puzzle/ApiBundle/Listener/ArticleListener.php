<?php 

namespace Puzzle\ApiBundle\Listener;

use Doctrine\ORM\EntityManagerInterface;
use Puzzle\ApiBundle\Entity\ArticleArchive;
use Puzzle\ApiBundle\Event\ArticleEvent;

/**
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 */
class ArticleListener
{
	/**
	 * @var EntityManagerInterface
	 */
	private $em;
	
	public function __construct(EntityManagerInterface $em){
		$this->em = $em;
	}
	
	public function onCreateArticle(ArticleEvent $event){
	    $article = $event->getArticle();
	    
	    $now = new \DateTime();
	    $archive = $this->em->getRepository(ArticleArchive::class)->findOneBy([
	        'month' => (int) $now->format("m"),
	        'year' => $now->format("Y")
	    ]);
	    
	    if ($archive === null) {
	        $archive = new ArticleArchive();
	        $archive->setMonth((int) $now->format("m"));
	        $archive->setYear($now->format("Y"));
	        
	        $this->em->persist($archive);
	    }
	    
	    $article->setArchive($archive);
	    $this->em->flush();
	    
	    return;
	}
}

?>
