<?php
namespace Puzzle\ApiBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Timestampable
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 * 
 * @ORM\HasLifecycleCallbacks()
 */
trait Timestampable
{
    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @JMS\Expose
	 * @JMS\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @JMS\Expose
	 * @JMS\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;
    
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(){
    	$this->createdAt = new \DateTime();
    }
    
    public function getCreatedAt(){
    	return $this->createdAt;
    }
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(){
    	$this->updatedAt = new \DateTime();
    }
    
    public function getUpdatedAt(){
        return $this->updatedAt;
    }
    
    public function convertInterval(){
        $interval = date_diff($this->updatedAt, new \DateTime("now"));
        $dateString = null;
        
        if (($interval->format('%a')) > 0) {
            $dateString = $interval->format('%a jrs');
        }elseif (($interval->format('%H')) > 0) {
            $dateString = $interval->format('%H h');
        }elseif ($interval->format("%i") > 0) {
            $dateString = $interval->format('%i mn');
        }else {
            $dateString = $interval->format('%is s');
        }
        
        return $dateString;
    }
}
