<?php
namespace Puzzle\ApiBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Sluggable
 * 
 * @author AGNES Gnagne Cedric <cecnho55@gmail.com>
 * 
 */
trait Sluggable
{
    /**
     * @ORM\Column(name="slug", type="string", nullable=true, unique=true)
     * @var string
     * @JMS\Expose
	 * @JMS\Type("string")
     */
    private $slug;
    
    public function setSlug(string $slug) {
        $this->slug = $slug;
        return $this;
    }
    
    public function getSlug(){
        return $this->slug;
    }
}
