<?php
namespace Puzzle\ApiBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Ownerable
 *
 * @author AGNES Gnagne Cedric <cecenho55@gmail@com>
 * 
 * @Hateoas\Relation(
 * 		name = "owner", 
 * 		href = @Hateoas\Route(
 * 			"get_user", 
 * 			parameters = {"id" = "expr(object.getUser())"},
 * 			absolute = true,
 * ))
 */
trait Ownerable
{
    /**
     * @var string
     * @ORM\Column(name="user", type="string", nullable=true)
     * @JMS\Expose
	 * @JMS\Type("string")
     */
    private $user;
    
    public function setUser(string $user) : self {
        $this->user = $user;
        return $this;
    }
    
    public function getUser() :? string {
        return $this->user;
    }
}
