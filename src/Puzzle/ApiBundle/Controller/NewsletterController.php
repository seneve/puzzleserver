<?php

namespace Puzzle\ApiBundle\Controller;

use Puzzle\ApiBundle\Service\Repository;
use Puzzle\ApiBundle\Util\FormatUtil;
use Symfony\Bridge\Doctrine\RegistryInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Puzzle\ApiBundle\Service\ErrorFactory;
use Puzzle\ApiBundle\Entity\NewsletterSubscriber;
use Puzzle\ApiBundle\Entity\NewsletterGroup;
use Puzzle\ApiBundle\Entity\NewsletterTemplate;

/**
 * Newsletter API
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 */
class NewsletterController extends BaseFOSRestController
{
    /**
     * @param RegistryInterface         $doctrine
     * @param Repository                $repository
     * @param SerializerInterface       $serializer
     * @param EventDispatcherInterface  $dispatcher
     * @param ErrorFactory              $errorFactory
     */
    public function __construct(
        RegistryInterface $doctrine,
        Repository $repository,
        SerializerInterface $serializer,
        EventDispatcherInterface $dispatcher,
        ErrorFactory $errorFactory
    ){
        parent::__construct($doctrine, $repository, $serializer, $dispatcher, $errorFactory);
    }
    
	/**
	 * @Annotations\View()
	 * @Get("/subscribers")
	 */
	public function getNewsletterSubscribersAction(Request $request) {
	    $response = $this->repository->filter($request->query, NewsletterSubscriber::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/subscribers/{id}")
	 * @ParamConverter("subscriber", class="PuzzleApiBundle:NewsletterSubscriber")
	 */
	public function getNewsletterSubscriberAction(Request $request, NewsletterSubscriber $subscriber) {
	    if ($subscriber->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $subscriber]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/subscribers")
	 */
	public function postNewsletterSubscriberAction(Request $request) {
	    $data = $request->request->all();
	    $subscriber = $this->serializer->deserialize(json_encode($data), NewsletterSubscriber::class,'json');
	    $subscriber->setUser($this->getUser()->getId());
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->persist($subscriber);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $subscriber]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/subscribers/{id}")
	 * @ParamConverter("subscriber", class="PuzzleApiBundle:NewsletterSubscriber")
	 */
	public function deleteNewsletterSubscriberAction(Request $request, NewsletterSubscriber $subscriber) {
	    if ($subscriber->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($subscriber);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/****************************** Group **********************************/
	
	/**
	 * @Annotations\View()
	 * @Get("/groups")
	 */
	public function getNewsletterGroupsAction(Request $request) {
	    $response = $this->repository->filter($request->query, NewsletterGroup::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/groups/{id}")
	 * @ParamConverter("group", class="PuzzleApiBundle:NewsletterGroup")
	 */
	public function getNewsletterGroupAction(Request $request, NewsletterGroup $group) {
	    if ($group->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $group]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/groups")
	 */
	public function postNewsletterGroupAction(Request $request) {
	    $data = $request->request->all();
	    $group = $this->serializer->deserialize(json_encode($data), NewsletterGroup::class,'json');
	    $group->setUser($this->getUser()->getId());
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->persist($group);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $group]));
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/groups/{id}")
	 * @ParamConverter("group", class="PuzzleApiBundle:NewsletterGroup")
	 */
	public function putNewsletterGroupAction(Request $request, NewsletterGroup $group) {
	    if ($group->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $data = $request->request->all();
	    
	    $oldArray = $this->repository->findField("*", $group->getId(), NewsletterGroup::class);
	    $arrayMerge = array_replace($oldArray[0], $data);
	    $arrayMerge['subscribers'] = FormatUtil::refreshArray([
	        'toAdd' => $data['subscribers_to_add'] ?? null,
	        'toRemove' => $data['subscribers_to_remove'] ?? null,
	        'old' => $group->getContacts()
	    ]);
	    $group = $this->serializer->deserialize(json_encode($arrayMerge), NewsletterGroup::class,'json');
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->merge($group);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/groups/{id}")
	 * @ParamConverter("group", class="PuzzleApiBundle:NewsletterGroup")
	 */
	public function deleteNewsletterGroupAction(Request $request, NewsletterGroup $group) {
	    if ($group->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($group);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	
	/*************************** Template *********************************/
	
	/**
	 * @Annotations\View()
	 * @Get("/templates")
	 */
	public function getNewsletterTemplatesAction(Request $request) {
	    $response = $this->repository->filter($request->query, NewsletterTemplate::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/templates/{id}")
	 * @ParamConverter("template", class="PuzzleApiBundle:NewsletterTemplate")
	 */
	public function getNewsletterTemplateAction(Request $request, NewsletterTemplate $template) {
	    if ($template->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $template]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/templates")
	 */
	public function postNewsletterTemplateAction(Request $request) {
	    $data = $request->request->all();
	    $template = $this->serializer->deserialize(json_encode($data), NewsletterTemplate::class,'json');
	    $template->setUser($this->getUser()->getId());
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->persist($template);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $template]));
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/templates/{id}")
	 * @ParamConverter("contact", class="PuzzleApiBundle:NewsletterTemplate")
	 */
	public function putNewsletterTemplateAction(Request $request, NewsletterTemplate $template) {
	    if ($template->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $oldArray = $this->repository->findField("*", $template->getId(), NewsletterTemplate::class);
	    $arrayMerge = array_replace($oldArray[0], $request->request->all());
	    $template = $this->serializer->deserialize(json_encode($arrayMerge), NewsletterTemplate::class,'json');
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->merge($template);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	
	/**
	 * @Annotations\View()
	 * @Delete("/templates/{id}")
	 * @ParamConverter("template", class="PuzzleApiBundle:NewsletterTemplate")
	 */
	public function deleteNewsletterTemplateAction(Request $request, NewsletterTemplate $template) {
	    if ($template->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($template);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
}