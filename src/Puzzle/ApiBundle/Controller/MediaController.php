<?php

namespace Puzzle\ApiBundle\Controller;

use Puzzle\ApiBundle\Service\Repository;
use Puzzle\ApiBundle\Util\FormatUtil;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Puzzle\OAuthServerBundle\Entity\User;
use Puzzle\OAuthServerBundle\UserEvents;
use Puzzle\OAuthServerBundle\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Puzzle\ApiBundle\Entity\MediaFile;
use Puzzle\ApiBundle\Service\MediaManager;
use Puzzle\ApiBundle\Service\MediaUploader;
use Puzzle\ApiBundle\Entity\MediaFolder;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Puzzle\ApiBundle\PuzzleApiEvents;
use Puzzle\ApiBundle\Event\MediaFolderEvent;
use Puzzle\ApiBundle\Event\MediaFileEvent;
use Puzzle\ApiBundle\Service\ErrorFactory;
use Puzzle\ApiBundle\Entity\MediaPicture;
use Puzzle\ApiBundle\Entity\MediaAudio;
use Puzzle\ApiBundle\Entity\MediaVideo;
use Puzzle\ApiBundle\Entity\MediaDocument;

/**
 * Media API
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 *
 */
class MediaController extends BaseFOSRestController
{
    /**
     * @var MediaManager
     */
    protected $mediaManager;
    
    /**
     * @var MediaUploader
     */
    protected $mediaUploader;
    
    /**
     * @param RegistryInterface         $doctrine
     * @param Repository                $repository
     * @param SerializerInterface       $serializer
     * @param EventDispatcherInterface  $dispatcher
     * @param ErrorFactory              $errorFactory
     * @param MediaManager              $mediaManager
     * @param MediaUploader             $mediaUploader
     */
    public function __construct(
        RegistryInterface $doctrine,
        Repository $repository,
        SerializerInterface $serializer,
        ErrorFactory $errorFactory,
        EventDispatcherInterface $dispatcher,
        MediaManager $mediaManager,
        MediaUploader $mediaUploader
    ){
        $this->mediaManager = $mediaManager;
        $this->mediaUploader = $mediaUploader;
        
        parent::__construct($doctrine, $repository, $serializer, $dispatcher, $errorFactory);
    }
    
	/**
	 * @Annotations\View()
	 * @Get("/files")
	 */
	public function getMediaFilesAction(Request $request) {
	    // Add filter by user_id
	    $filter = $request->query->get('filter');
	    $filter = $filter ? $filter.',user=='.$this->getUser()->getId() : 'user=='.$this->getUser()->getId();
	    $request->query->set('filter', $filter);
	    
	    $response = $this->repository->filter($request->query, MediaFile::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/folders")
	 */
	public function getMediaFoldersAction(Request $request) {
	    // Add filter by user_id
	    $filter = $request->query->get('filter');
	    $filter = $filter ? $filter.',user=='.$this->getUser()->getId() : 'user=='.$this->getUser()->getId();
	    $request->query->set('filter', $filter);
	    
	    $response = $this->repository->filter($request->query, MediaFolder::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/files/{id}")
	 * @ParamConverter("user", class="PuzzleApiBundle:MediaFile")
	 */
	public function getMediaFileAction(Request $request, MediaFile $file) {
	    if ($file->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $file]));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/folders/{id}")
	 * @ParamConverter("user", class="PuzzleApiBundle:MediaFolder")
	 */
	public function getMediaFolderAction(Request $request, MediaFolder $folder) {
	    if ($folder->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $folder]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/files")
	 */
	public function postMediaFileAction(Request $request) {
	    $user = $this->getUser();
	    $data = $request->request->all();
	    $em = $this->doctrine->getManager($this->connection);
	    
	    if (isset($data['folder']) === false || $data['folder'] === null) {
	        $folder = $em->getRepository(MediaFolder::class)->findOneBy([
	            'name'             => $user->getUsername(),
	            'user'             => $user->getId(),
	            'isOverwritable'   => false
	        ]);
	        
	        if ($folder === null) {
	            $folder = new MediaFolder();
	            $folder->createDefault($user);
	            
	            $em->persist($folder);
	            $em->flush($folder);
	        }
	        
	        $data['folder'] = $folder->getId();
	    }
		
	    $dataUploadFromUrl = $this->mediaUploader->uploadFromUrl($data['url'], $data['folder']);
		$data = array_merge($data, $dataUploadFromUrl);
		
		/** @var MediaFile $file */
		$file = $this->serializer->deserialize(json_encode($data), MediaFile::class,'json');
		$file->setUser($this->getUser()->getId());
		
		$em->persist($file);
		
		$folder = $em->getRepository(MediaFolder::class)->find($data['folder']);
		$folder->addFile($file->getId());
		
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['resources' => $file]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/folders")
	 */
	public function postMediaFolderAction(Request $request) {
	    $data = $request->request->all();
	    $em = $this->doctrine->getManager($this->connection);
	    
	    $parent = isset($data['parent']) && $data['parent'] ? $em->getRepository(MediaFolder::class)->find($data['parent']) : null;
	    $data['is_overwritable'] = $data['is_overwritable'] ?? true;
	    
	    $folder = $this->serializer->deserialize(json_encode($data), MediaFolder::class,'json');
	    $folder->setUser($this->getUser()->getId());
	    $folder->setParent($parent);
	    
	    $em->persist($folder);
	    $em->flush();
	    
	    $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_CREATE_FOLDER, new MediaFolderEvent($folder, [
	        'user' => $this->getUser()
	    ]));
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $folder]));
	}
	
	
	/**
	 * @Annotations\View()
	 * @Put("/files/{id}")
	 * @ParamConverter("file", class="PuzzleApiBundle:MediaFile")
	 */
	public function putMediaFileAction(Request $request, MediaFile $file) {
	    if ($file->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $data = $request->request->all();
		
	    $oldAbsolutePath = null;
	    if (isset($data['name']) && $data['name'] !== null){
	        $oldAbsolutePath = $file->getAbsolutePath();
	        $file->setName($data['name']);
	    }
	    
	    if (isset($data['caption']) && $data['caption'] !== null){
	        $file->setName($data['caption']);
	    }
	    
		$em = $this->doctrine->getManager($this->connection);
		$em->flush();
		
		if ($oldAbsolutePath !== $file->getAbsolutePath()){
		    $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_RENAME_FILE, new MediaFileEvent([
		        'oldAbsolutePath' => $oldAbsolutePath,
		        'absolutePath' => $file->getAbsolutePath()
		    ]));
		}
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));	
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/folders/{id}")
	 * @ParamConverter("folder", class="PuzzleApiBundle:MediaFolder")
	 */
	public function putMediaFolderAction(Request $request, MediaFolder $folder) {
	    if ($folder->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $data = $request->request->all();
	    
	    $oldAbsolutePath = null;
	    if (isset($data['name']) && $data['name'] !== null){
	        $oldAbsolutePath = $folder->getAbsolutePath();
	        $folder->setName($data['name']);
	    }
	    
	    $filesToAdd = $data['files_to_add'] ?? null;
	    $filesToRemove = $data['files_to_remove'] ?? null;
	    $files = FormatUtil::refreshArray($folder->getFiles(), $filesToAdd, $filesToRemove);
	    
	    $folder->setFiles($files);
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->flush();
	    
	    if ($oldAbsolutePath !== $folder->getAbsolutePath()){
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_RENAME_FOLDER, new MediaFolderEvent($folder, [
	            'oldAbsolutePath' => $oldAbsolutePath
	        ]));
	    }
	    
	    if ($filesToAdd !== null) {
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_ADD_FILES_TO_FOLDER, new MediaFolderEvent($folder, [
	            'files_to_add'     => $filesToAdd,
	            'preserve_files'   => $data['preserve_files'] ?? true,
	            'user'             => $this->getUser()
	        ]));
	    }
	    
	    if ($filesToRemove !== null){
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_REMOVE_FILES_TO_FOLDER, new MediaFolderEvent($folder, [
	            'files_to_remove'  => $filesToRemove,
	            'user'             => $this->getUser()
	        ]));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/files/{id}")
	 * @ParamConverter("file", class="PuzzleApiBundle:MediaFile")
	 */
	public function deleteMediaFileAction(Request $request, MediaFile $file) {
	    if ($file->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_REMOVE_FILE, new MediaFileEvent([
	        'absolutePath' => $file->getAbsolutePath()
	    ]));
	    
		$em = $this->doctrine->getManager($this->connection);
		$em->remove($file);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/folders/{id}")
	 * @ParamConverter("folder", class="PuzzleApiBundle:MediaFolder")
	 */
	public function deleteMediaFolderAction(Request $request, MediaFolder $folder) {
	    if ($folder->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_REMOVE_FOLDER, new MediaFolderEvent($folder));
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($folder);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
}