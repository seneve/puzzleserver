<?php

namespace Puzzle\ApiBundle\Controller;

use Puzzle\ApiBundle\Service\Repository;
use Puzzle\ApiBundle\Util\FormatUtil;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Puzzle\OAuthServerBundle\Entity\User;
use Puzzle\OAuthServerBundle\UserEvents;
use Puzzle\OAuthServerBundle\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Puzzle\ApiBundle\Service\ErrorFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Oauth server user
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 *
 */
class UserController extends BaseFOSRestController
{
    /**
     * @param RegistryInterface         $doctrine
     * @param Repository                $repository
     * @param SerializerInterface       $serializer
     * @param EventDispatcherInterface  $dispatcher
     * @param ErrorFactory              $errorFactory
     */
    public function __construct(
        RegistryInterface $doctrine,
        Repository $repository,
        SerializerInterface $serializer,
        EventDispatcherInterface $dispatcher,
        ErrorFactory $errorFactory
    ){
        parent::__construct($doctrine, $repository, $serializer, $dispatcher, $errorFactory);
    }
    
	/**
	 * @Annotations\View()
	 * @Get("/users")
	 */
	public function getUsersAction(Request $request) {
	    $response = $this->repository->filter($request->query, User::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/users/me")
	 */
	public function getUserMeAction(Request $request) {
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $this->getUser()]));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/users/{id}")
	 * @ParamConverter("user", class="PuzzleOAuthServerBundle:User")
	 */
	public function getUserAction(Request $request, User $user) {
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $user]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/users")
	 */
	public function postUserAction(Request $request) {
		$data = $request->request->all();
		$data['enabled'] = $data['enabled'] ?? true;
		$data['locked'] = $data['locked'] ?? false;
		$data['password_changed'] = $data['password_changed'] ?? false;
		$data['roles'] = $data['roles'] ?? User::ROLE_DEFAULT;
		$data['roles'] = explode(',', strtoupper($data['roles']));
		
		$user = $this->serializer->deserialize(json_encode($data),User::class,'json');
		$user->setPassword(hash('sha512', $data['password']));
		
		$em = $this->doctrine->getManager($this->connection);
		$em->persist($user);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['resources' => $user]));
	}
	
	
	/**
	 * @Annotations\View()
	 * @Put("/users/{id}")
	 * @ParamConverter("user", class="PuzzleOAuthServerBundle:User")
	 */
	public function putUserAction(Request $request, User $user) {
		$oldArray = $this->repository->findField("*", $user->getId(), User::class);
		$arrayMerge = array_replace($oldArray[0], $request->request->all());
		$arrayMerge['roles'] = $arrayMerge['roles'] ?? User::ROLE_DEFAULT;
		$arrayMerge['roles'] = explode(',', strtoupper($arrayMerge['roles']));
		
		$user = $this->serializer->deserialize(json_encode($arrayMerge),User::class,'json');
		
		$em = $this->doctrine->getManager($this->connection);
		$em->merge($user);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));	
	}
	
	
	/**
	 * @Annotations\View()
	 * @Delete("/users/{id}")
	 * @ParamConverter("user", class="PuzzleOAuthServerBundle:User")
	 */
	public function deleteUserAction(Request $request, User $user) {
		$em = $this->doctrine->getManager($this->connection);
		$em->remove($user);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
}