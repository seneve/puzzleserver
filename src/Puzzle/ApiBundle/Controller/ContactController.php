<?php

namespace Puzzle\ApiBundle\Controller;

use Puzzle\ApiBundle\Service\Repository;
use Puzzle\ApiBundle\Util\FormatUtil;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Puzzle\ApiBundle\Entity\Contact;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Puzzle\ApiBundle\PuzzleApiEvents;
use Puzzle\ApiBundle\Event\MediaFolderEvent;
use Puzzle\ApiBundle\Event\MediaFileEvent;
use Puzzle\ApiBundle\Util\MediaUtil;
use Puzzle\ApiBundle\Entity\ContactGroup;
use Puzzle\ApiBundle\Service\ErrorFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Contact API
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 */
class ContactController extends BaseFOSRestController
{
    /**
     * @param RegistryInterface         $doctrine
     * @param Repository                $repository
     * @param SerializerInterface       $serializer
     * @param EventDispatcherInterface  $dispatcher
     * @param ErrorFactory              $errorFactory
     */
    public function __construct(
        RegistryInterface $doctrine,
        Repository $repository,
        SerializerInterface $serializer,
        EventDispatcherInterface $dispatcher,
        ErrorFactory $errorFactory
    ){
        parent::__construct($doctrine, $repository, $serializer, $dispatcher, $errorFactory);
    }
    
	/**
	 * @Annotations\View()
	 * @Get("/contacts")
	 */
	public function getContactsAction(Request $request) {
	    // Add filter by user_id
	    $filter = $request->query->get('filter');
	    $filter = $filter ? $filter.',user=='.$this->getUser()->getId() : 'user=='.$this->getUser()->getId();
	    $request->query->set('filter', $filter);
	    
	    $response = $this->repository->filter($request->query, Contact::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/groups")
	 */
	public function getContactGroupsAction(Request $request) {
	    // Add filter by user_id
	    $filter = $request->query->get('filter');
	    $filter = $filter ? $filter.',user=='.$this->getUser()->getId() : 'user=='.$this->getUser()->getId();
	    $request->query->set('filter', $filter);
	    
	    $response = $this->repository->filter($request->query, ContactGroup::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/contacts/{id}")
	 * @ParamConverter("contact", class="PuzzleApiBundle:Contact")
	 */
	public function getContactAction(Request $request, Contact $contact) {
	    if ($contact->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $contact]));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/groups/{id}")
	 * @ParamConverter("group", class="PuzzleApiBundle:ContactGroup")
	 */
	public function getContactGroupAction(Request $request, ContactGroup $group) {
	    if ($group->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $group]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/contacts")
	 */
	public function postContactAction(Request $request) {
		$data = $request->request->all();
		$contact = $this->serializer->deserialize(json_encode($data),Contact::class,'json');
		$contact->setUser($this->getUser()->getId());
		
		$em = $this->doctrine->getManager($this->connection);
		$em->persist($contact);
		
		if ($picture = $contact->getPicture()){
		    $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_COPY_FILE, new MediaFileEvent([
		        'path'        => $picture,
		        'folder'      => MediaUtil::extractContext(Contact::class),
		        'user'        => $this->getUser(),
		        'closure'     => function($filename) use ($contact){$contact->setPicture($filename);}
		    ]));
		}
		
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['resources' => $contact]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/groups")
	 */
	public function postContactGroupAction(Request $request) {
	    $data = $request->request->all();
	    $em = $this->doctrine->getManager($this->connection);
	    // Initialize parent
	    $parent = null;
	    // Get parent
	    if (isset($data['parent']) && $data['parent']){
	        $parent = $em->getRepository(ContactGroup::class)->find($data['parent']);
	    }
	    /** @var ContactGroup $group */
	    $group = $this->serializer->deserialize(json_encode($data),ContactGroup::class,'json');
	    $group->setUser($this->getUser()->getId());
	    $group->setParent($parent);
	    
	    $em->persist($group);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $group]));
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/contacts/{id}")
	 * @ParamConverter("contact", class="PuzzleApiBundle:Contact")
	 */
	public function putContactAction(Request $request, Contact $contact) {
	    if ($contact->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    $data = $request->request->all();
	    // Prepare for picture updated
	    $updatePicture = isset($data['picture']) && $data['picture'] !== $contact->getPicture() ? true : false;
	    
	    $oldArray = $this->repository->findField("*", $contact->getId(), Contact::class);
		$arrayMerge = array_replace($oldArray[0], $request->request->all());
		$contact = $this->serializer->deserialize(json_encode($arrayMerge), Contact::class,'json');
		
		if ($updatePicture === true){
		    $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_COPY_FILE, new MediaFileEvent([
		        'path'        => $contact->getPicture(),
		        'folder'      => MediaUtil::extractContext(Contact::class),
		        'user'        => $this->getUser(),
		        'closure'     => function($filename) use ($contact){$contact->setPicture($filename);}
		    ]));
		}
		
		$em = $this->doctrine->getManager($this->connection);
		$em->merge($contact);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));	
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/groups/{id}")
	 * @ParamConverter("group", class="PuzzleApiBundle:ContactGroup")
	 */
	public function putContactGroupAction(Request $request, ContactGroup $group) {
	    if ($group->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $data = $request->request->all();
	    
	    $contactsToAdd = $data['contacts_to_add'] ?? null;
	    $contactsToRemove = $data['contacts_to_remove'] ?? null;
	    $contacts = FormatUtil::refreshArray($group->getContacts(), $contactsToAdd, $contactsToRemove);
	    
	    $oldArray = $this->repository->findField("*", $group->getId(), ContactGroup::class);
	    $arrayMerge = array_replace($oldArray[0], $data);
	    $arrayMerge['contacts'] = $contacts;
	    
	    $group = $this->serializer->deserialize(json_encode($arrayMerge), ContactGroup::class,'json');
	    // Update parent
	    if (isset($data['parent']) && $data['parent'] !== null){
	        $parent = $em->getRepository(ContactGroup::class)->find($data['parent']);
	        $group->setParent($parent);
	    }
	    
	    $em->merge($group);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/contacts/{id}")
	 * @ParamConverter("contact", class="PuzzleApiBundle:Contact")
	 */
	public function deleteContactAction(Request $request, Contact $contact) {
	    if ($contact->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    if ($contact->getPicture() !== null){
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_REMOVE_FILE, new MediaFileEvent([
	            'absolutePath' => $contact->getPicture()
	        ]));
	    }
	    
		$em = $this->doctrine->getManager($this->connection);
		$em->remove($contact);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/groups/{id}")
	 * @ParamConverter("group", class="PuzzleApiBundle:ContactGroup")
	 */
	public function deleteContactGroupAction(Request $request, ContactGroup $group) {
	    if ($group->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($group);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
}