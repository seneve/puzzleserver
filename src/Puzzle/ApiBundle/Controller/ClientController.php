<?php

namespace Puzzle\ApiBundle\Controller;

use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use Puzzle\ApiBundle\Service\Repository;
use Puzzle\ApiBundle\Util\FormatUtil;
use Puzzle\OAuthServerBundle\Entity\Client;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use JMS\Serializer\SerializerInterface;
use Puzzle\ApiBundle\Service\ErrorFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Oauth server client
 * 
 * @author qwincy <qwincypercy@fermentuse.com>
 *
 */
class ClientController extends BaseFOSRestController
{
    /**
     * @var ClientManagerInterface $clientManager
     */
    protected $clientManager;
    
    /**
     * @param RegistryInterface         $doctrine
     * @param Repository                $repository
     * @param SerializerInterface       $serializer
     * @param EventDispatcherInterface  $dispatcher
     * @param ErrorFactory              $errorFactory
     */
    public function __construct(
        RegistryInterface $doctrine,
        Repository $repository,
        SerializerInterface $serializer,
        EventDispatcherInterface $dispatcher,
        ErrorFactory $errorFactory,
        ClientManagerInterface $clientManager
    ){
        $this->clientManager = $clientManager;
        parent::__construct($doctrine, $repository, $serializer, $dispatcher, $errorFactory);
    }
    
	/**
	 * @Annotations\View()
	 * @Get("/clients")
	 */
	public function getOauthClientsAction(Request $request) {
	    $request->query->add(['user' => $this->getUser()->getId()]);
	    // Add filter by user_id
	    $filter = $request->query->get('filter');
	    $filter = $filter ? $filter.',user=='.$this->getUser()->getId() : 'user=='.$this->getUser()->getId();
	    $request->query->set('filter', $filter);
	    
	    $response = $this->repository->filter($request->query, Client::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/clients/{publicId}")
	 */
	public function getOauthClientAction(Request $request, $publicId) {
	    $client = $this->clientManager->findClientByPublicId($publicId);
	    
	    if (!$client){
	        return $this->handleView($this->errorFactory->notFound($request));
	    }
	    
	    if ($client->getUser()->getId() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $client]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/clients")
	 */
	public function postOauthClientAction(Request $request) {
		$redirectUris = explode(',', $request->request->get('redirect_uris'));
		$grantTypes = explode(',', $request->request->get('grant_types'));
		$name = $request->request->get('name');
		$user = $this->getUser()->getId();
		
		$client = $this->clientManager->createClient();
		$client->setName($name);
		$client->setUser($user);
		$client->setRedirectUris($redirectUris);
		$client->setAllowedGrantTypes($grantTypes);
		
		$this->clientManager->updateClient($client);
		
		$em = $this->doctrine->getManager($this->connection);
		$em->persist($client);
		$em->flush();
		
		$array = [
			'client_id' => $client->getRandomId(),
			'client_secret' => $client->getSecret(),
			'name' => $client->getName(),
			'redirect_uris' => $redirectUris,
			'grant_types' => $grantTypes
		];
        
		return $this->handleView(FormatUtil::formatView($request, $array));
	}
	
	
	/**
	 * @Annotations\View()
	 * @Put("/clients/{publicId}")
	 */
	public function putOauthClientAction(Request $request, $publicId) {
	    $client = $this->clientManager->findClientByPublicId($publicId);
	    
	    if (!$client){
	        return $this->handleView($this->errorFactory->notFound($request));
	    }
	    
	    if ($client->getUser()->getId() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
		
		$oldArray = $this->repository->findField("*", $client->getId(), Client::class);
		$arrayMerge = array_replace($oldArray[0], $request->request->all());
		$createdAt = $client->getCreatedAt();
		
		$client = $this->serializer->deserialize(json_encode($arrayMerge),Client::class,'json');
		$client->setCreatedAt($createdAt);
		$client->setUpdatedAt(new \DateTime());
		
		$em = $this->doctrine->getManager($this->connection);
		$em->merge($client);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));	
	}
	
	
	/**
	 * @Annotations\View()
	 * @Delete("/clients/{publicId}")
	 */
	public function deleteOauthClientAction(Request $request, $publicId) {
	    $client = $this->clientManager->findClientByPublicId($publicId);
	    
	    if (!$client){
	        return $this->handleView($this->errorFactory->notFound($request));
	    }
	    
	    if ($client->getUser()->getId() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
		
		$em = $this->doctrine->getManager($this->connection);
		$em->remove($client);
		$em->flush();
		
		return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
}