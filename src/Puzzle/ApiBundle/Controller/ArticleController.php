<?php

namespace Puzzle\ApiBundle\Controller;

use Puzzle\ApiBundle\Service\Repository;
use Puzzle\ApiBundle\Util\FormatUtil;
use Symfony\Bridge\Doctrine\RegistryInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Puzzle\ApiBundle\Service\ErrorFactory;
use Puzzle\ApiBundle\Entity\NewsletterSubscriber;
use Puzzle\ApiBundle\Entity\NewsletterGroup;
use Puzzle\ApiBundle\Entity\NewsletterTemplate;
use Puzzle\ApiBundle\Entity\Article;
use Puzzle\ApiBundle\Entity\ArticleCategory;
use Puzzle\ApiBundle\PuzzleApiEvents;
use Puzzle\ApiBundle\Event\MediaFileEvent;
use Puzzle\ApiBundle\Util\MediaUtil;
use Puzzle\ApiBundle\Entity\ArticleComment;
use Puzzle\ApiBundle\Entity\ArticleArchive;
use Puzzle\ApiBundle\Event\ArticleEvent;
use Puzzle\ApiBundle\Entity\MediaFolder;

/**
 * Article API
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 */
class ArticleController extends BaseFOSRestController
{
    /**
     * @param RegistryInterface         $doctrine
     * @param Repository                $repository
     * @param SerializerInterface       $serializer
     * @param EventDispatcherInterface  $dispatcher
     * @param ErrorFactory              $errorFactory
     */
    public function __construct(
        RegistryInterface $doctrine,
        Repository $repository,
        SerializerInterface $serializer,
        EventDispatcherInterface $dispatcher,
        ErrorFactory $errorFactory
    ){
        parent::__construct($doctrine, $repository, $serializer, $dispatcher, $errorFactory);
    }
    
	/**
	 * @Annotations\View()
	 * @Get("/articles")
	 */
	public function getArticlesAction(Request $request) {
	    // Add filter by user_id
	    $filter = $request->query->get('filter');
	    $filter = $filter ? $filter.',user=='.$this->getUser()->getId() : 'user=='.$this->getUser()->getId();
	    $request->query->set('filter', $filter);
	    
	    $response = $this->repository->filter($request->query, Article::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/articles/{id}")
	 * @ParamConverter("article", class="PuzzleApiBundle:Article")
	 */
	public function getArticleAction(Request $request, Article $article) {
	    if ($article->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $article]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/articles")
	 */
	public function postArticleAction(Request $request) {
	    $data = $request->request->all();
	    $data['tags'] = isset($data['tags']) && $data['tags'] !== null ? explode(',', $data['tags']) : null;
	    $data['enable_comments'] = $data['enable_comments'] ?? false;
	    $data['visible'] = $data['visible'] ?? true;
	    $data['author'] = $data['author'] ?? $this->getUser()->getFullName();
	    /** @var Article $article */
	    $article = $this->serializer->deserialize(json_encode($data), Article::class,'json');
	    $article->setUser($this->getUser()->getId());
	    
	    $em = $this->doctrine->getManager($this->connection);
	    
	    $category = $em->getRepository(ArticleCategory::class)->find($data['category']);
	    $article->setCategory($category);
	    
	    
	    $em->persist($article);
	    $em->flush($article);
	    
	    // Archive article
	    $this->dispatcher->dispatch(PuzzleApiEvents::ARTICLE_CREATE, new ArticleEvent($article));
	    // Add picture
	    if ($picture = $article->getPicture()){
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_COPY_FILE, new MediaFileEvent([
	            'path'     => $picture,
	            'folder'   => MediaUtil::extractContext(ArticleCategory::class),
	            'user'     => $this->getUser(),
	            'closure'  => function($filename) use ($article){$article->setPicture($filename);}
	        ]));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $article]));
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/articles/{id}")
	 * @ParamConverter("article", class="PuzzleApiBundle:Article")
	 */
	public function putArticleAction(Request $request, Article $article) {
	    if ($article->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $data = $request->request->all();
	    // Format tags
	    $data['tags'] = isset($data['tags']) && $data['tags'] !== null ? explode(',', $data['tags']) : null;
	    // Prepare for picture updated
	    $updatePicture = isset($data['picture']) && $data['picture'] !== $article->getPicture() ? true : false;
	    // Serialize data
	    $oldArray = $this->repository->findField("*", $article->getId(), Article::class);
	    $arrayMerge = array_replace($oldArray[0], $data);
	    
	    $article = $this->serializer->deserialize(json_encode($arrayMerge), Article::class,'json');
	    // Update category
	    if (isset($data['category']) && $data['category'] !== null){
	        $category = $em->getRepository(ArticleCategory::class)->find($data['category']);
	        $article->setCategory($category);
	    }
	    // Update picture
	    if ($updatePicture === true){
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_COPY_FILE, new MediaFileEvent([
	            'path'     => $article->getPicture(),
	            'folder'   => MediaUtil::extractContext(Article::class),
	            'user'     => $this->getUser(),
	            'closure'  => function($filename) use ($article){$article->setPicture($filename);}
	        ]));
	    }
	    // Apply updates
	    $em->merge($article);
	    $em->flush();
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/articles/{id}")
	 * @ParamConverter("article", class="PuzzleApiBundle:Article")
	 */
	public function deleteArticleAction(Request $request, Article $article) {
	    if ($article->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($article);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/****************************** Category **********************************/
	
	/**
	 * @Annotations\View()
	 * @Get("/categories")
	 */
	public function getArticleCategoriesAction(Request $request) {
	    // Add filter by user_id
	    $filter = $request->query->get('filter');
	    $filter = $filter ? $filter.',user=='.$this->getUser()->getId() : 'user=='.$this->getUser()->getId();
	    $request->query->set('filter', $filter);
	    
	    $response = $this->repository->filter($request->query, ArticleCategory::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/categories/{id}")
	 * @ParamConverter("category", class="PuzzleApiBundle:ArticleCategory")
	 */
	public function getArticleCategoryAction(Request $request, ArticleCategory $category) {
	    if ($category->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $category]));
	}
	
	/**
	 * @Annotations\View()
	 * @Post("/categories")
	 */
	public function postArticleCategoryAction(Request $request) {
	    $em = $this->doctrine->getManager($this->connection);
	    $data = $request->request->all();
	    // Initialize parent
	    $parent = null;
	    // Get parent
	    if (isset($data['parent']) && $data['parent']){
	        $parent = $em->getRepository(ArticleCategory::class)->find($data['parent']);
	    }
	    /** @var ArticleCategory $category */
	    $category = $this->serializer->deserialize(json_encode($data), ArticleCategory::class,'json');
	    $category->setUser($this->getUser()->getId());
	    $category->setParent($parent);
	    
	    $em->persist($category);
	    // Add picture
	    if ($picture = $category->getPicture()){
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_COPY_FILE, new MediaFileEvent([
	            'path'     => $picture,
	            'folder'   => MediaUtil::extractContext(ArticleCategory::class),
	            'user'     => $this->getUser(),
	            'closure'  => function($filename) use ($category){$category->setPicture($filename);}
	        ]));
	    }
	    
	    $em->flush();
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $category]));
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/categories/{id}")
	 * @ParamConverter("category", class="PuzzleApiBundle:ArticleCategory")
	 */
	public function putArticleCategoryAction(Request $request, ArticleCategory $category) {
	    if ($category->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $data = $request->request->all();
	    // For updating picture
	    $picture = $data['picture'] ?? null;
	    $updatePicture = $picture !== null && $picture !== $category->getPicture() ? true : false;
	    // Serialize data
	    $oldArray = $this->repository->findField("*", $category->getId(), ArticleCategory::class);
	    $arrayMerge = array_replace($oldArray[0], $data);
	    $category = $this->serializer->deserialize(json_encode($arrayMerge), ArticleCategory::class,'json');
	    // Update parent
	    if (isset($data['parent']) && $data['parent'] !== null){
	        $parent = $em->getRepository(ArticleCategory::class)->find($data['parent']);
	        $category->setParent($parent);
	    }
	   // Update picture
	    if ($updatePicture === true){
	        $this->dispatcher->dispatch(PuzzleApiEvents::MEDIA_COPY_FILE, new MediaFileEvent([
	            'path'     => $picture,
	            'folder'   => MediaUtil::extractContext(ArticleCategory::class),
	            'user'     => $this->getUser(),
	            'closure'  => function($filename) use ($category){$category->setPicture($filename);}
	        ]));
	    }
	    
	    $em->merge($category);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	/**
	 * @Annotations\View()
	 * @Delete("/categories/{id}")
	 * @ParamConverter("category", class="PuzzleApiBundle:ArticleCategory")
	 */
	public function deleteArticleCategoryAction(Request $request, ArticleCategory $category) {
	    if ($category->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($category);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	
	/*************************** Archive *********************************/
	
	/**
	 * @Annotations\View()
	 * @Get("/archives")
	 */
	public function getArticleArchivesAction(Request $request) {
	    $response = $this->repository->filter($request->query, ArticleArchive::class, $this->connection);
	    return $this->handleView(FormatUtil::formatView($request, $response));
	}
	
	/**
	 * @Annotations\View()
	 * @Get("/archives/{id}")
	 * @ParamConverter("archive", class="PuzzleApiBundle:ArticleArchive")
	 */
	public function getArticleArchiveAction(Request $request, ArticleArchive $archive) {
	    if ($archive->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->accessDenied($request));
	    }
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $archive]));
	}
	
	/*************************** Comment *********************************/
	
	/**
	 * @Annotations\View()
	 * @Post("/comments")
	 */
	public function postArticleCommentAction(Request $request) {
	    $data = $request->request->all();
	    // Initiliaze is_visble
	    $data['is_visible'] = $data['is_visible'] ?? false;
	    // Serialize data
	    $comment = $this->serializer->deserialize(json_encode($data), ArticleComment::class,'json');
	    $comment->setUser($this->getUser()->getId());
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->persist($comment);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['resources' => $comment]));
	}
	
	/**
	 * @Annotations\View()
	 * @Put("/comments/{id}")
	 * @ParamConverter("comment", class="PuzzleApiBundle:ArticleComment")
	 */
	public function putArticleCommentAction(Request $request, ArticleComment $comment) {
	    if ($comment->getUser() !== $this->getUser()->getId() && $comment->getArticle()->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    // Serialize data
	    $oldArray = $this->repository->findField("*", $comment->getId(), ArticleComment::class);
	    $arrayMerge = array_replace($oldArray[0], $request->request->all());
	    $comment = $this->serializer->deserialize(json_encode($arrayMerge), ArticleComment::class,'json');
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->merge($comment);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
	
	
	/**
	 * @Annotations\View()
	 * @Delete("/comments/{id}")
	 * @ParamConverter("comment", class="PuzzleApiBundle:ArticleComment")
	 */
	public function deleteArticleCommentAction(Request $request, ArticleComment $comment) {
	    if ($comment->getUser() !== $this->getUser()->getId() && $comment->getArticle()->getUser() !== $this->getUser()->getId()){
	        return $this->handleView($this->errorFactory->badRequest($request));
	    }
	    
	    $em = $this->doctrine->getManager($this->connection);
	    $em->remove($comment);
	    $em->flush();
	    
	    return $this->handleView(FormatUtil::formatView($request, ['code' => 200]));
	}
}