<?php

namespace Puzzle\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PuzzleApiBundle:Default:index.html.twig');
    }
}
