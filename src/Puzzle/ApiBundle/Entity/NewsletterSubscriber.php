<?php

namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Puzzle\ApiBundle\Traits\PrimaryKeyable;
use Puzzle\ApiBundle\Traits\Ownerable;
use Puzzle\ApiBundle\Traits\Nameable;

/**
 * Newsletter Subscriber
 *
 * @ORM\Table(name="newsletter_subscriber")
 * @ORM\Entity()
 * @JMS\ExclusionPolicy("all")
 * @JMS\XmlRoot("newsletter_subscriber")
 * @Hateoas\Relation(
 * 		name = "self", 
 * 		href = @Hateoas\Route(
 * 			"get_newsletter_subscriber", 
 * 			parameters = {"id" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 */
class NewsletterSubscriber
{
    use PrimaryKeyable,
        Nameable,
        Ownerable;
    
    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=255)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $email;
    
    public function setEmail($email) : self {
        $this->email = $email;
        return $this;
    }

    public function getEmail() :? string {
        return $this->email;
    }
}
