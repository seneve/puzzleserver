<?php
namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Doctrine\Common\Collections\Collection;
use Puzzle\ApiBundle\Traits\PrimaryKeyable;
use Puzzle\ApiBundle\Traits\Timestampable;
use Puzzle\ApiBundle\Traits\Nameable;
use Puzzle\ApiBundle\Traits\Describable;
use Puzzle\ApiBundle\Traits\Pictureable;
use Puzzle\ApiBundle\Traits\Ownerable;
use Puzzle\ApiBundle\Traits\Sluggable;

/**
 * Article Category
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 *
 * @ORM\Table(name="blog_category")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @JMS\ExclusionPolicy("all")
 * @JMS\XmlRoot("article_category")
 * @Hateoas\Relation(
 * 		name = "self", 
 * 		href = @Hateoas\Route(
 * 			"get_article_category", 
 * 			parameters = {"id" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 * @Hateoas\Relation(
 * 		name = "articles", 
 * 		href = @Hateoas\Route(
 * 			"get_articles", 
 * 			parameters = {"category" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 * @Hateoas\Relation(
 *     name = "parent",
 *     embedded = "expr(object.getParent())",
 *     exclusion = @Hateoas\Exclusion(excludeIf = "expr(object.getParent() === null)"),
 *     href = @Hateoas\Route(
 * 			"get_media_folder", 
 * 			parameters = {"id" = "expr(object.getParent().getId())"},
 * 			absolute = true,
 * ))
 * @Hateoas\Relation(
 *     name = "childs",
 *     embedded = "expr(object.getChilds())",
 *     exclusion = @Hateoas\Exclusion(excludeIf = "expr(object.getChilds() === null)")
 * ))
 */
class ArticleCategory
{
    use PrimaryKeyable,
        Timestampable,
        Nameable,
        Describable,
        Pictureable,
        Ownerable,
        Sluggable;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category")
     */
    private $articles;
    
    /**
     * @ORM\OneToMany(targetEntity="ArticleCategory", mappedBy="parent")
     */
    private $childs;
    
    /**
     * @ORM\ManyToOne(targetEntity="ArticleCategory", inversedBy="childs")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;
    
    /**
     * Constructor
     */
    public function __construct() {
    	$this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function setArticles(Collection $articles) : self {
        $this->articles = $articles;
        return $this;
    }
    
    public function addArticle(Article $article) : self {
        if ($this->articles === null || $this->articles->contains($article) === false){
            $this->articles->add($article);
        }
        
        return $this;
    }

    public function removeArticle(Article $article) : self {
        $this->articles->removeElement($article);
        return $this;
    }

    public function getArticles(){
        return $this->articles;
    }

    public function setParent(ArticleCategory $parent = null){
        $this->parent = $parent;
        return $this;
    }

    public function getParent(){
        return $this->parent;
    }

    public function addChild(ArticleCategory $child) : self {
        $this->childs[] = $child;
        return $this;
    }

    public function removeChild(ArticleCategory $child) : self{
        $this->childs->removeElement($child);
        return $this;
    }

    public function getChilds() :? Collection{
        return $this->childs;
    }
}
