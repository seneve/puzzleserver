<?php

namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Puzzle\ApiBundle\Traits\PrimaryKeyable;
use Puzzle\ApiBundle\Traits\Describable;
use Puzzle\ApiBundle\Traits\Nameable;
use Puzzle\ApiBundle\Traits\Ownerable;
use Puzzle\ApiBundle\Traits\Timestampable;
use Doctrine\Common\Collections\Collection;

/**
 * Newsletter Group
 *
 * @ORM\Table(name="newsletter_group")
 * @ORM\Entity()
 * @JMS\ExclusionPolicy("all")
 * @JMS\XmlRoot("newsletter_group")
 * @Hateoas\Relation(
 * 		name = "self",
 * 		href = @Hateoas\Route(
 * 			"get_newsletter_group",
 * 			parameters = {"id" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 */
class NewsletterGroup
{
    use PrimaryKeyable,
    Describable,
    Nameable,
    Ownerable,
    Timestampable;
    /**
     * @var array
     * @ORM\Column(name="subscribers", type="array", nullable=true)
     * @JMS\Expose
     * @JMS\Type("array")
     */
    private $subscribers;
    
    public function setSubscribers($subscribers) : self {
        foreach ($subscribers as $subscriber){
            $this->addSubscriber($subscriber);
        }
        
        return $this;
    }
    
    public function addSubscriber($subscriber) : self {
        $this->subscribers[] = $subscriber;
        $this->subscribers = array_unique($this->subscribers);
        
        return $this;
    }
    
    public function removeSubscriber($subscriber) : self {
        $this->subscribers = array_diff($this->subscribers, [$subscriber]);
        return $this;
    }
    
    public function getSubscribers() :? array {
        return $this->subscribers;
    }
}