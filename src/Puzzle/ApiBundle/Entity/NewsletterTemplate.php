<?php

namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Puzzle\ApiBundle\Traits\PrimaryKeyable;
use Puzzle\ApiBundle\Traits\Nameable;
use Puzzle\ApiBundle\Traits\Ownerable;

/**
 * Template
 *
 * @ORM\Table(name="newsletter_template")
 * @ORM\Entity()
 * @JMS\ExclusionPolicy("all")
 * @JMS\XmlRoot("newsletter_template")
 * @Hateoas\Relation(
 * 		name = "self",
 * 		href = @Hateoas\Route(
 * 			"get_newsletter_template",
 * 			parameters = {"id" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 */
class NewsletterTemplate
{
    use PrimaryKeyable,
        Nameable,
        Ownerable;

    /**
     * @var string
     * @ORM\Column(name="trigger", type="string", length=255)
     * @JMS\Expose
     * @JMS\Type("array")
     */
    private $trigger;
    
    /**
     * @var string
     * @ORM\Column(name="content", type="text")
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $content;
   
    public function setTrigger($trigger) : self {
        $this->trigger = $trigger;
        return $this;
    }
    
    public function getTrigger() :? string {
        return $this->trigger;
    }
    
    public function setContent($content) : self {
        $this->content = $content;
        return $this;
    }
    
    public function getContent() :? string {
        return $this->content;
    }
}
