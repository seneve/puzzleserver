<?php
namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Puzzle\ApiBundle\Traits\Timestampable;
use Puzzle\ApiBundle\Traits\Ownerable;
use Puzzle\ApiBundle\Traits\PrimaryKeyable;

/**
 * Article Comment
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 * 
 * @ORM\Table(name="article_comment")
 * @ORM\Entity()
 * @JMS\ExclusionPolicy("all")
 * @JMS\XmlRoot("article_comment")
 */
class ArticleComment
{
    use PrimaryKeyable,
        Timestampable,
        Ownerable;
    
    /**
     * @var string
     * @ORM\Column(name="content", type="text")
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $content;

    /**
     * @var bool
     * @ORM\Column(name="is_visible", type="boolean")
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $isVisible;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;
    
    public function setContent($content) : self {
        $this->content = $content;
        return $this;
    }

    public function getContent() :? string {
        return $this->content;
    }

    public function setIsVisible($isVisible) : self {
        $this->isVisible = $isVisible;
        return $this;
    }

    public function getIsVisible() : bool {
        return $this->isVisible;
    }

    
    public function setArticle(Article $article = null){
        $this->article = $article;
        return $this;
    }

    public function getArticle() : Article {
        return $this->article;
    }
}
