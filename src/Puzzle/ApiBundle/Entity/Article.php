<?php
namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Puzzle\ApiBundle\Traits\PrimaryKeyable;
use Puzzle\ApiBundle\Traits\Timestampable;
use Puzzle\ApiBundle\Traits\Nameable;
use Puzzle\ApiBundle\Traits\Ownerable;
use Puzzle\ApiBundle\Traits\Sluggable;
use Puzzle\ApiBundle\Traits\Describable;
use Puzzle\ApiBundle\Traits\Pictureable;
use Puzzle\ApiBundle\Traits\Taggable;

/**
 * Article
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 *
 * @ORM\Table(name="article")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @JMS\ExclusionPolicy("all")
 * @JMS\XmlRoot("article")
 * @Hateoas\Relation(
 * 		name = "self", 
 * 		href = @Hateoas\Route(
 * 			"get_article", 
 * 			parameters = {"id" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 * @Hateoas\Relation(
 *     name = "category",
 *     embedded = "expr(object.getCategory())",
 *     exclusion = @Hateoas\Exclusion(excludeIf = "expr(object.getCategory() === null)"),
 *     href = @Hateoas\Route(
 * 			"get_article_category", 
 * 			parameters = {"id" = "expr(object.getCategory().getId())"},
 * 			absolute = true,
 * ))
 * @Hateoas\Relation(
 *     name = "archive",
 *     embedded = "expr(object.getArchive())",
 *     exclusion = @Hateoas\Exclusion(excludeIf = "expr(object.getArchive() === null)")
 * )
 * @Hateoas\Relation(
 *     name = "comments",
 *     embedded = "expr(object.getComments())",
 *     exclusion = @Hateoas\Exclusion(excludeIf = "expr(object.getComments() === null)")
 * )
 */
class Article
{
    use PrimaryKeyable,
        Timestampable,
        Nameable,
        Ownerable,
        Sluggable,
        Describable,
        Pictureable,
        Taggable;
    
    /**
     * @ORM\Column(name="short_description", type="string", length=255, nullable=true)
     * @var string
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $shortDescription;
    
    /**
     * @ORM\Column(name="enable_comments", type="boolean")
     * @var boolean
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $enableComments;
    
    /**
     * @ORM\Column(name="visible", type="boolean")
     * @var boolean
     * @JMS\Expose
     * @JMS\Type("boolean")
     */
    private $visible;
    
    /**
     * @ORM\Column(name="author", type="string")
     * @var string
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $author;
   
    /**
     * @ORM\ManyToOne(targetEntity="ArticleCategory", inversedBy="articles")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @ORM\ManyToOne(targetEntity="ArticleArchive", inversedBy="articles")
     * @ORM\JoinColumn(name="archive_id", referencedColumnName="id")
     */
    private $archive;
    
    /**
     * @ORM\OneToMany(targetEntity="ArticleComment", mappedBy="article")
     */
    private $comments;
    
    /**
     * Constructor
     */
    public function __construct() {
    	$this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->visible = true;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setShortDescription(){
        $this->shortDescription = strlen($this->description) > 100 ? 
                                  mb_strimwidth($this->description, 0, 100, '...') : $this->description;
        return $this;
    }

    public function getShortDescription() :? string {
        return $this->shortDescription;
    }

    public function setEnableComments($enableComments) : self {
        $this->enableComments = $enableComments;
        return $this;
    }

    public function getEnableComments() :? bool {
        return $this->enableComments;
    }
    
    public function setVisible(bool $visible) : self {
        $this->visible = $visible;
        return $this;
    }
    
    public function isVisible() :? bool {
        return $this->visible;
    }
    
    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }
    
    public function getAuthor() {
        return $this->author;
    }
    
    public function setCategory(ArticleCategory $category) : self {
        $this->category = $category;
        return $this;
    }

    public function getCategory() :? ArticleCategory {
        return $this->category;
    }
    
    public function setArchive(ArticleArchive $archive) : self {
        $this->archive = $archive;
        return $this;
    }
    
    public function getArchive() :? ArticleArchive {
        return $this->archive;
    }

    public function addComment(ArticleComment $comment) : self {
        if ($this->comments === null || $this->comments->contains($comment) === false) {
            $this->comments->add($comment);
        }
        
        return $this;
    }

    public function removeComment(ArticleComment $comment) : self {
        $this->comments->removeElement($comment);
    }

    public function getComments() :? Collection {
        return $this->comments;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function urlizeSlug(){
        $this->slug = $this->category === null ? $this->slug : $this->category->getSlug().'/'.$this->slug;
    }
    
    
    public function __toString(){
        return $this->getName();
    }
}
