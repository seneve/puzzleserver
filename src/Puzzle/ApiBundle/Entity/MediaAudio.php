<?php

namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Puzzle\ApiBundle\Traits\PrimaryKeyable;

/**
 * Media Audio 
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 *
 * @ORM\Table(name="media_audio")
 * @ORM\Entity(repositoryClass="Puzzle\ApiBundle\Repository\MediaAudioRepository")
 * @ORM\HasLifecycleCallbacks()
 * @JMS\XmlRoot("audio")
 * @Hateoas\Relation(
 * 		name = "self", 
 * 		href = @Hateoas\Route(
 * 			"get_media_audio", 
 * 			parameters = {"id" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 */
class MediaAudio
{
    use PrimaryKeyable;
    
    /**
     * @ORM\OneToOne(targetEntity="MediaFile", inversedBy="audio")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private $file;

    public function setFile(MediaFile $file = null) : self {
        $this->file = $file;
        return $this;
    }

    public function getFile() :? MediaFile {
        return $this->file;
    }
}
