<?php

namespace Puzzle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Hateoas\Configuration\Annotation as Hateoas;

use Puzzle\ApiBundle\Traits\PrimaryKeyable;

/**
 * Video
 * 
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 *
 * @ORM\Table(name="media_video")
 * @ORM\Entity(repositoryClass="Puzzle\ApiBundle\Repository\MediaVideoRepository")
 * @ORM\HasLifecycleCallbacks()
 * @JMS\XmlRoot("video")
 * @Hateoas\Relation(
 * 		name = "self", 
 * 		href = @Hateoas\Route(
 * 			"get_media_video", 
 * 			parameters = {"id" = "expr(object.getId())"},
 * 			absolute = true,
 * ))
 */
class MediaVideo
{   
    use PrimaryKeyable;

    /**
     * @ORM\OneToOne(targetEntity="MediaFile", inversedBy="video")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private $file;

    public function setFile(MediaFile $file = null) : self {
        $this->file = $file;
        return $this;
    }

    public function getFile() :? MediaFile {
        return $this->file;
    }
}
