# Newsletter APIs
## 
---
## Subscriber API
---

> **Entity properties** 
- id  (*string*)
- name (*string*)
- email (*string*)
- user
> **Fetch subscribers**
```php
GET /{version}/newsletter/subscribers{_format}
```
Return files collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
> **Fetch a  subscriber**    
```php
GET /{version}/newsletter/subscribers/{id}{_format}
```

Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .

> **Add a new  subscriber**
```php
POST /{version}/newsletter/subscribers{_format}
```

Fields which can be set are:
- name
- email
Return code
- **success** : It returns a *subscriber object*
- **error** : it returns error *code* and error *message*.

> **Edit a subscriber**
```php
PUT /{version}/newsletter/subscribers/{id}{_format}
```

Fields which can be edit are:
- name
- email
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
> **Delete a subscriber**
```php
DELETE /{version}/newsletter/subscribers/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.

---
## Subscriber Group API
---

> **Entity properties** 
- id  (*string*)
- name (*string*)
- subscribers (*string*)
- user
> **Fetch subscriber groups**
```php
GET /{version}/newsletter/groups{_format}
```
    
Return files collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
> **Fetch a subscriber group**
```php
GET /{version}/newsletter/groups{id}{_format}
```
Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .

> **Create a new subscriber group**
```php
POST /{version}/newsletter/groups{_format}
```

Fields which can be set are:
- name
Return code
- **success** : It returns a *group object*
- **error** : it returns error *code* and error *message*.
> **Edit a subscriber group**
```php
PUT /{version}/newsletter/groups/{id}{_format}
```

Fields which can be edit are:
- name
- subscribers_to_add: list of urls or ids of subscribers to add 
- subscribers_to_remove: list of ids of subscribers to remove 
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
> **Delete a subscriber group**
```php
DELETE /{version}/newsletter/groups/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.

---
## Template API
---

> **Entity properties** 
- id  (*string*)
- name (*string*)
- content (*string*)
- trigger (string): event name associated to this template
- user
> **Fetch subscribers**
```php
GET /{version}/newsletter/templates{_format}
```
Return files collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
> **Fetch a  template**    
```php
GET /{version}/newsletter/templates/{id}{_format}
```

Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .

> **Add a new  template**
```php
POST /{version}/newsletter/templates{_format}
```

Fields which can be set are:
- name
- content
- trigger
Return code
- **success** : It returns a *template object*
- **error** : it returns error *code* and error *message*.

> **Edit a template**
```php
PUT /{version}/newsletter/templates/{id}{_format}
```

Fields which can be edit are:
- name
- content
- trigger
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
> **Delete a template**
```php
DELETE /{version}/newsletter/templates/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
