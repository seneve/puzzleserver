# Client API

---
## Entity properties 
---
- **id**  (***string***): client id
- **random** (***string***) : random value used for generating client public id
- **secret (*string*)** : client secret
- **name** (***string***): client screen name
- **host (*string*)**: client host name. Delimites the domain of redirect uris.
- **redirect_uris (array)**: client redirect uris
- **grant_types (array)**: client allowed grant types (see [OAuth2](https://oauth.net/2/))
- **is_interne** (***bool***): If `interne=true`, authorization is not required. If `interne=false`, authorization is required
- user : client owner
NB : client public id is generating by: `client_id = id.'_'.random`
---
## Fetch clients
---
```php
    GET /{version}/clients{_format}
```
    
    Return client collection for current user into given format. 
- **id**
- **client_id**
- **client_secret**
- **name**
- **host**
- **redirect_uris**
- **grant_types (array)**
- **is_interne**
    For filtering, sorting or paginating, see ***Query parameters.md*** .
    
---
## Fetch a client
---
```php
    GET /{version}/clients/{id}{_format}
```
Return client  into given format. 
- **id**
- **client_id**
- **client_secret**
- **name**
- **host**
- **redirect_uris**
- **grant_types (array)**
- **is_interne**
    For filtering, sorting or paginating, see ***Query parameters.md*** .
---
## Create a new client
---
```php
    POST /{version}/clients{_format}
```

Fields which can be set are:
- **name**
- **host**
- **redirect_uris**
- **grant_types (array)**
- **is_interne**
Return code
- **success** : It returns a *client_id* and *client_secret*
- **error** : it returns error *code* and error *message*.

---
## Edit a client
---
```php
    PUT /{version}/clients/{id}{_format}
```

Fields which can be edit are:
- **name**
- **host**
- **redirect_uris**
- **grant_types (array)**
- **is_interne**
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
---
## Delete a client
---
```php
    DELETE /{version}/clients/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.