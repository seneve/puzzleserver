# QUERY PARAMETERS
---

## > **Filter results (filter)**

It is used to apply a filter to the collection of excepted objects. It can be used with all fields of the entity.

### **1. Comparison Operators**
- **[field]=@[value]** : fetch objects with field  *[field]* contains value *[value]*
E.g: Fetch all objects with *name* contains *iphone* : `filter=name=@iphone`
- **[field]!@[value]** : fetch objects with field  *[field]* doesn't contain value *[value]*
E.g: Fetch all objects with *name* doesn't contains *iphone* : `filter=name!@iphone`
- **[field]=^[value]** : fetch objects with field  *[field]* begins by value *[value]*
E.g: Fetch all objects with *name* begins by *iphone* : `filter=name=^iphone`
- **[field]=$[value]** : fetch objects with field *[field]* ends by value *[value]*
E.g: Fetch all objects with *name* ends by *iphone* : `filter=name=$iphone`
- **[field]=:[value1];[value2];...;[valueN]** : fetch objects with field  *[field]* value is in list of values *[value1];[value2];...;[valueN]*
E.g: Fetch all objects with *id* value is in list of values *(1,2,3)* : `filter=id=:1;2;3`
- **[field]==[value]** : fetch objects with field *[field]* equals to value *[value]*
E.g: Fetch all objects with *price* equals to *10000* : `filter=price==10000`
- **[field]=![value]** : fetch objects with field *[field]* not equals to or different to value *[value]*
E.g: Fetch all objects with *price* not equals to *10000* : `filter=price=!10000`
- **[field]>[value]** : fetch objects with field *[field]* greater than value *[value]*
E.g: Fetch all objects with *price* greater than *10000* : `filter=price>10000`
- **[field]>=[value]** : fetch objects with field *[field]* greater or equal than value *[value]*
E.g: Fetch all objects with *price* greater or equal than *10000* : `filter=price>=10000`
- **[field]<[value]** : fetch objects with field *[field]* lower than value *[value]*
E.g: Fetch all objects with *price* lower than *10000* : `filter=price<10000`
- **[field]>=[value]** : fetch objects with field *[field]* lower or equal than value *[value]*
E.g: Fetch all objects with *price* lower or equal than *10000* : `filter=price<=10000`

### **2. Logic Operators**
- **OR : [condition1]|[condition2]|...|[conditionN]** : fetch objects validate [condition1] or [condition2]... or [conditionN]
E.g: Fetch all objects with *name* contains *iphone* **or** *price* lower or equal than *10000* : `filter=name=@iphone|price<=10000`
- **AND : [condition1],[condition2],...,[conditionN]** : fetch objects validate [condition1] and [condition2]... and [conditionN]
E.g: Fetch all objects with *name* contains *iphone* **and** *price* lower or equal than *10000* : `filter=name=@iphone|price<=10000`


---
## > **Filter fields (fields)**

It is used to apply a filter to the fields that will be visible. It can be used with all fields of the entity.
The syntax is as follows: **fields=[champ1],[champ2],...,[champN]**

E.g : Show only id and name fields : `fields=id,name`


---
## > **Sorting results (orderBy)**

It is used to sort results. It can be used with all fields of the entity. The syntax is as follows: **orderBy=[field]:[order]**
Possible values for [order] are:
- **ASC**: for ascending (from the smallest to the largest)
- **DESC**: for descendant (from the largest to the smallest)
E.g: Order results by price descendant: `orderBy=price:DESC`

---
## > **Paginate results (limit & page)**

It is used to limit the number results for a given page. The syntax is as follows :  **limit=[value]&page=[page_number]**
By default `page_number=1`
E.g: Return 10 results from page 5: `limit=10&page=5`

