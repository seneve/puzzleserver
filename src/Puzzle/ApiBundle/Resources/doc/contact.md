# Contact APIs
## 
---
## Contact API
---

> **Entity properties** 
- id  (*string*)
- first_name (*string*)
- last_name (*string*)
- email (*string*)
- gender (*string*)
- phone (*string*)
- location (*string*)
- company (*string*)
- position (*string*)
- created_at (*datetime*)
- updated_at (*datetime*)
- user
> **Fetch contacts**
```php
GET /{version}/contacts{_format}
```
Return files collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
> **Fetch a contact**    
```php
GET /{version}/contacts/{id}{_format}
```

Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .

> **Create a new contact**
```php
POST /{version}/contacts{_format}
```

Fields which can be set are:
- first_name
- last_name
- email
- gender
- phone
- location
- company
- position
Return code
- **success** : It returns a *contact object*
- **error** : it returns error *code* and error *message*.

> **Edit a contact**
```php
PUT /{version}/contacts/{id}{_format}
```

Fields which can be edit are:
- first_name
- last_name
- email
- gender
- phone
- location
- company
- position
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
> **Delete a contact**
```php
DELETE /{version}/contacts/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.

---
## Contact Group API
---

> **Entity properties** 
- id  (*string*)
- name (*string*)
- contacts (*string*)
- parent
- user
> **Fetch contact group**
```php
GET /{version}/contact-groups{_format}
```
    
Return files collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
> **Fetch a folder**
```php
GET /{version}/contact-groups{id}{_format}
```
Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .

> **Create a new contact group**
```php
POST /{version}/contact-groups{_format}
```

Fields which can be set are:
- name
Return code
- **success** : It returns a *group object*
- **error** : it returns error *code* and error *message*.
> **Edit a contact group**
```php
PUT /{version}/contact-groups/{id}{_format}
```

Fields which can be edit are:
- name
- contacts_to_add: list of urls or ids of contacts to add 
- contacts_to_remove: list of ids of contacts to remove 
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
> **Delete a contact group**
```php
DELETE /{version}/contact-groups/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.