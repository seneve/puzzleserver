# Media APIs
## 
---
## Media File API
---


> **Entity properties** 
- id  (*string*)
- name (*string*)
- caption (*string*)
- path (*string*)
- extension (*string*)
- size (int)
- created_at (datetime)
- updated_at (datetime)
- user
> **Fetch files**
```php
GET /{version}/media/files{_format}
```
Return files collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
> **Fetch a file**    
```php
GET /{version}/media/files/{id}{_format}
```

Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .

> **Import a new file**
```php
POST /{version}/media/files{_format}
```

Fields which can be set are:
- **url**
- **name**
- **caption**
- **folder:** folder id where stores file. If it is empty, default folder is used.
Return code
- **success** : It returns a *file object*
- **error** : it returns error *code* and error *message*.

> **Edit a file**
```php
PUT /{version}/media/files/{id}{_format}
```

Fields which can be edit are:
- **name**
- **caption**
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
> **Delete a file**
```php
DELETE /{version}/media/files/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.

---
## Media Folder API
---

> **Entity properties** 
- id  (*string*)
- name (*string*)
- app_name (*string*)
- path (*string*)
- allowed_extensions (*array*)
- description (*string*)
- tag (*string*)
- slug (*string*)
- created_at (datetime)
- updated_at (datetime)
- parent
- user
> **Fetch folders**
```php
GET /{version}/media/folders{_format}
```
    
Return files collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
> **Fetch a folder**
```php
GET /{version}/media/folders/{id}{_format}
```
Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .

> **Create a new folder**
```php
POST /{version}/media/folders{_format}
```

Fields which can be set are:
- url
- name
- app_name
- description
- tag
- allowed_extensions
- parent
Return code
- **success** : It returns a *folder object*
- **error** : it returns error *code* and error *message*.
> **Edit a folder**
```php
PUT /{version}/media/folders/{id}{_format}
```

Fields which can be edit are:
- **name**
- **app_name**
- **description**
- **tag**
- **allowed_extensions**
- **parent**
- **files_to_add:** list of urls or ids of file to add 
- **files_to_remove**: list of ids of file to remove 
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
> **Delete a folder**
```php
DELETE /{version}/media/folders/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.