# User API

---
## Entity properties 
---
- **id**  (***string***)
- **first_name** (***string***)
- **last_name (*string*)**
- **email** (***string***)
- **gender (*string*)**
- **phone (string)**
- **username (string)** 
- **password (*string*)**
- **enabled** (**bool**)
- **locked** **(*bool*)**
- **roles** **(array):** useful to defined what user can do.
- **account_expires_at (datetime)**
- **credentials_expires_at (datetime)**
- **password_changed:** useful to force user to change its password after first login

---
## Fetch users
---
```php
    GET /{version}/users{_format}
```
    
Return client collection for current user into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
    
---
## Fetch a user
---
```php
    GET /{version}/users/{id}{_format}
```
Return client  into given format. 
For filtering, sorting or paginating, see ***Query parameters.md*** .
---
## Create a new user
---
```php
    POST /{version}/users{_format}
```

Fields which can be set are:
- **first_name**
- **last_name**
- **email**
- **gender**
- **phone**
- **username**
- **password**
- **enabled**
- **locked**
- **roles**
- **account_expires_at**
- **credentials_expires_at**
Return code
- **success** : It returns a *user object*
- **error** : it returns error *code* and error *message*.

---
## Edit a user
---
```php
    PUT /{version}/users/{id}{_format}
```

Fields which can be edit are:
- **first_name**
- **last_name**
- **email**
- **gender**
- **phone**
- **username**
- **password**
- **enabled**
- **locked**
- **roles**
- **account_expires_at**
- **credentials_expires_at**
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.
---
## Delete a user
---
```php
    DELETE /{version}/users/{id}{_format}
```
Return
- **success** : code 200
- **error** : error *code* and error *message*  are sent.