<?php 

namespace Puzzle\ApiBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Puzzle\ApiBundle\Entity\MediaFolder;

/**
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 */
class MediaFolderEvent extends Event
{
	/**
	 * @var MediaFolder
	 */
	private $folder;
	
	/**
	 * @var array
	 */
	private $data;
	
	public function __construct(MediaFolder $folder, array $data = null){
		$this->folder= $folder;
		$this->data = $data;
	}
	
	public function getFolder(){
		return $this->folder;
	}
	
	public function getData(){
	    return $this->data;
	}
}

?>