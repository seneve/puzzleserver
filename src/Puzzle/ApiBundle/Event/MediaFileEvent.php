<?php 

namespace Puzzle\ApiBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * @author AGNES Gnagne Cedric <cecenho55@gmail.com>
 */
class MediaFileEvent extends Event
{
	/**
	 * @var array
	 */
	private $data;
	
	public function __construct(array $data){
		$this->data = $data;
	}
	
	public function getData(){
	    return $this->data;
	}
}

?>